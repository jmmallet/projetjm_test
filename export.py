import  pickle
import csv


def mef_klines(klines):
    compteur=1
    for it in klines:
        # On supprime les derniers éléments
        del it[-1]
        # On divise par 1000 les champs correspondand
        # a "open_time" et "close_time" qui seront utilisé
        # en tan que "DATETIME"
        it[0]/=1000
        it[6]/=1000
        it.insert(0,compteur)
        compteur +=1
    return klines

def export_to_cvs(iobject, filename):

    # On met en forme l'iobject
    iobject=mef_klines(iobject)
    print(iobject)

    with open(filename, 'w', newline='') as csvfile:

        spamwriter = csv.writer(csvfile)
        for ligne in iobject:
            spamwriter.writerow(ligne)


# On récupère l'objet depuit le fichier
with open('klines-DEC-2020.dat', 'rb') as fd:
    t=pickle.load(fd)

export_to_cvs(t,"toto")